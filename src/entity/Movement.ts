import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { Length, IsOptional, IsPositive, Matches } from 'class-validator'
import { Type } from 'class-transformer'

import { IMovement } from '../model/IMovement'
import Category from './Category'
import EntityBase from './EntityBase'

@Entity()
export default class Movement extends EntityBase implements IMovement {
  @PrimaryGeneratedColumn('uuid')
  id?: string

  @Column({ length: 128, nullable: false })
  uid!: string

  @Column({ length: 50, nullable: false })
  @Length(3, 50)
  name!: string

  @Column({ length: 255, default: '' })
  @Length(0, 255)
  @IsOptional()
  description?: string

  @Column({ type: 'date' })
  @Matches(/^\d{4}-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$/, { message: 'Must be a valid date yyyy-mm-dd' })
  date!: string

  @Column({ type: 'double' })
  @IsPositive()
  amount!: number

  @Column()
  currency!: string

  @Column({ default: () => false })
  @IsOptional()
  income?: boolean

  @Column({ default: () => false })
  @IsOptional()
  done?: boolean

  @Column({ length: 255 })
  @IsOptional()
  coordinates?: string

  @Column()
  @IsOptional()
  feeNumber?: number

  @Column()
  @IsOptional()
  payday?: number

  @ManyToOne(() => Category, category => category.id)
  @Type(() => Category)
  category?: Category;
}
