import { Column, Entity, PrimaryColumn } from 'typeorm'
import { Length, IsInt, IsOptional } from 'class-validator'

import EntityBase from './EntityBase'
import { ICategory } from '../model/ICategory'

@Entity()
export default class Category extends EntityBase implements ICategory {
  @PrimaryColumn()
  @IsInt()
  id!: number

  @Column({ length: 50, nullable: false })
  @Length(3, 50)
  name!: string

  @Column({ length: 255, default: '' })
  @Length(0, 255)
  @IsOptional()
  description?: string

  @Column({ length: 30, nullable: false })
  @Length(3, 30)
  color!: string

  @Column({ length: 20, nullable: false })
  @Length(3, 20)
  icon!: string

  @Column({ default: () => false })
  @IsOptional()
  expense?: boolean

  @Column({ default: () => false })
  @IsOptional()
  income?: boolean
}
