import { Router } from 'express'

import CategoryService from '../service/CategoryService'

const router: Router = Router()
const categoryService = new CategoryService()

router.get('/', categoryService.findAll)
router.get('/:id([0-9]+)', categoryService.findById)
router.post('/', categoryService.create)
router.put('/:id([0-9]+)', categoryService.updateById)
router.delete('/:id([0-9]+)', categoryService.deleteById)

export default router
