import { Router, Request, Response } from 'express'

import categories from './categories'
import movements from './movements'
import auth from './auth'

const router = Router()

router.get('/', (_req: Request, res: Response) => {
  res.status(200).send({ message: 'Bienvenido a SimpleMonkey API' })
})

router.use('/categories', categories)
router.use('/movements', movements)
router.use('/auth', auth)

export default router
