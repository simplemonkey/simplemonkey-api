import { Router } from 'express'

import MovementService from '../service/MovementService'
import { verifyToken, verifyUid } from '../middleware/firebaseUserAuth'

const router: Router = Router()
const movementService = new MovementService()

router.get('/:uid/', verifyToken, verifyUid, movementService.findAll)
router.get('/:uid/last-six-months', verifyToken, verifyUid, movementService.findLastSixMonths)
router.get('/:uid/last-month', verifyToken, verifyUid, movementService.findLastMonth)
router.get('/:uid/last-ten', verifyToken, verifyUid, movementService.findLastTen)
router.get('/:uid/balance', verifyToken, verifyUid, movementService.getBalance)
router.get('/:uid/:id', verifyToken, verifyUid, movementService.findById)
router.post('/:uid/', verifyToken, verifyUid, movementService.create)
router.put('/:uid/:id', verifyToken, verifyUid, movementService.updateById)
router.delete('/:uid/:id', verifyToken, verifyUid, movementService.deleteById)


export default router
