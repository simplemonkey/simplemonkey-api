import { Router } from 'express'
import AuthService from '../service/AuthService'

const authService: AuthService = new AuthService()

const router: Router = Router()

router.post('/login', authService.login)
router.post('/register', authService.register)

export default router
