import { Response, Request } from 'express'
import { DeleteResult, UpdateResult } from 'typeorm'
import { validate } from 'class-validator'
import { plainToClass } from 'class-transformer'
import { isEmpty } from 'ramda'

import CategoryRepository from '../repository/CategoryRepository'
import AbstractService from './AbstractService'
import Category from '../entity/Category'

const categoryRepository = new CategoryRepository()

export default class CategoryService extends AbstractService {
  async create (req: Request, res: Response): Promise<void> {
    try {
      const newCategory: Category = plainToClass(Category, req.body)
      const errors = await validate(newCategory)
      if (isEmpty(errors)) {
        const data = await categoryRepository.create(newCategory)
        res.status(201).send(data)
      } else {
        res.status(400).send(errors)
      }
    } catch (err) {
      if (process.env.NODE_ENV !== 'test') { console.log(err.errno, err.message) }
      if (err.errno === 1062) {
        res.sendStatus(409)
      } else {
        res.sendStatus(500)
      }
    }
  }

  async findAll (_req: Request, res: Response): Promise<void> {
    try {
      const data: Category[] = await categoryRepository.getAll()
      res.status(200).send(data)
    } catch (err) {
      if (process.env.NODE_ENV !== 'test') { console.log(err.errno, err.message) }
      res.sendStatus(500)
    }
  }

  async findById (req: Request, res: Response): Promise<void> {
    try {
      const { id } = req.params
      const data: Category | undefined = await categoryRepository.getById(parseInt(id, 10))
      if (!data) {
        res.sendStatus(404)
      }
      res.status(200).send(data)
    } catch (err) {
      if (process.env.NODE_ENV !== 'test') { console.log(err.errno, err.message) }
      res.sendStatus(500)
    }
  }

  async updateById (req: Request, res: Response): Promise<void> {
    try {
      const { id } = req.params
      const category: Category = plainToClass(Category, { ...req.body, id: parseInt(id, 10) })
      const errors = await validate(category)
      if (isEmpty(errors)) {
        const data: UpdateResult = await categoryRepository.updateById(parseInt(id, 10), category)
        if (data.affected! > 0) { // TODO: Another way?
          res.sendStatus(204)
        } else {
          res.sendStatus(404)
        }
      } else {
        res.status(400).send(errors)
      }
    } catch (err) {
      if (process.env.NODE_ENV !== 'test') { console.log(err.errno, err.message) }
      res.sendStatus(500)
    }
  }

  async deleteById (req: Request, res: Response): Promise<void> {
    try {
      const { id } = req.params
      const data: DeleteResult = await categoryRepository.deleteById(parseInt(id, 10))
      if (data.affected! > 0) { // TODO: Another way?
        res.sendStatus(204)
      } else {
        res.sendStatus(404)
      }
    } catch (err) {
      if (process.env.NODE_ENV !== 'test') { console.log(err.errno, err.message) }
      res.sendStatus(500)
    }
  }
}
