import { Response, Request } from 'express'
import { DeleteResult, UpdateResult } from 'typeorm'
import { validate } from 'class-validator'
import { plainToClass } from 'class-transformer'
import { isEmpty } from 'ramda'

import MovementRepository from '../repository/MovementRepository'
import AbstractService from './AbstractService'
import Movement from '../entity/Movement'

const movementRepository = new MovementRepository()

export default class MovementService extends AbstractService {
  async create (req: Request, res: Response): Promise<void> {
    const { uid } = req.params
    try {
      const newMovement: Movement = plainToClass(Movement, { ...req.body, uid })
      const errors = await validate(newMovement)
      if (isEmpty(errors)) {
        const data = await movementRepository.create(newMovement)
        res.status(201).send(data)
      } else {
        res.status(400).send(errors)
      }
    } catch (err) {
      if (process.env.NODE_ENV !== 'test') { console.log(err.errno, err.message) }
      if (err.errno === 1062) {
        res.sendStatus(409)
      } else {
        res.sendStatus(500)
      }
    }
  }

  async findAll (req: Request, res: Response): Promise<void> {
    const { uid } = req.params
    try {
      const data: Movement[] = await movementRepository.getAll(uid)
      res.status(200).send(data)
    } catch (err) {
      if (process.env.NODE_ENV !== 'test') { console.log(err.errno, err.message) }
      res.sendStatus(500)
    }
  }

  async findById (req: Request, res: Response): Promise<void> {
    try {
      const { id, uid } = req.params
      const data: Movement | undefined = await movementRepository.getById(id, uid)
      if (!data) {
        res.sendStatus(404)
      }
      res.status(200).send(data)
    } catch (err) {
      if (process.env.NODE_ENV !== 'test') { console.log(err.errno, err.message) }
      res.sendStatus(500)
    }
  }

  async updateById (req: Request, res: Response): Promise<void> {
    try {
      const { id, uid } = req.params
      const movement: Movement = plainToClass(Movement, { ...req.body, uid })
      const errors = await validate(movement)
      if (isEmpty(errors)) {
        const data: UpdateResult = await movementRepository.updateById(id, movement)
        if (data.affected! > 0) { // TODO: Another way?
          res.sendStatus(204)
        } else {
          res.sendStatus(404)
        }
      } else {
        res.status(400).send(errors)
      }
    } catch (err) {
      if (process.env.NODE_ENV !== 'test') { console.log(err.errno, err.message) }
      res.sendStatus(500)
    }
  }

  async deleteById (req: Request, res: Response): Promise<void> {
    try {
      const { id } = req.params
      const data: DeleteResult = await movementRepository.deleteById(id)
      if (data.affected! > 0) { // TODO: Another way?
        res.sendStatus(204)
      } else {
        res.sendStatus(404)
      }
    } catch (err) {
      if (process.env.NODE_ENV !== 'test') { console.log(err.errno, err.message) }
      res.sendStatus(500)
    }
  }

  async findLastSixMonths (req: Request, res: Response): Promise<void> {
    const { uid } = req.params
    try {
      const data: Movement[] = await movementRepository.getLastSixMonths(uid)
      res.status(200).send(data)
    } catch (err) {
      if (process.env.NODE_ENV !== 'test') { console.log(err.errno, err.message) }
      res.sendStatus(500)
    }
  }

  async findLastTen (req: Request, res: Response): Promise<void> {
    const { uid } = req.params
    try {
      const data: Movement[] = await movementRepository.getLastTen(uid)
      res.status(200).send(data)
    } catch (err) {
      if (process.env.NODE_ENV !== 'test') { console.log(err.errno, err.message) }
      res.sendStatus(500)
    }
  }

  async findLastMonth (req: Request, res: Response): Promise<void> {
    const { uid } = req.params
    try {
      const data: Movement[] = await movementRepository.getLastMonth(uid)
      res.status(200).send(data)
    } catch (err) {
      if (process.env.NODE_ENV !== 'test') { console.log(err.errno, err.message) }
      res.sendStatus(500)
    }
  }

  async getBalance (req: Request, res: Response): Promise<void> {
    const { uid } = req.params
    try {
      const data = await movementRepository.getBalance(uid)
      res.status(200).send(data)
    } catch (err) {
      if (process.env.NODE_ENV !== 'test') { console.log(err.errno, err.message) }
      res.sendStatus(500)
    }
  }
}
