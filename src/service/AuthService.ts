import { Response, Request } from 'express'
import Axios from 'axios'
import { auth, database } from 'firebase-admin'

import { IAuthCredentials, IAuthPayload } from '../model/IAuth'

const firebaseAuthErrors: { [key: string]: number } = {
  'auth/email-already-exists': 409,
  'auth/invalid-email': 400
}

export default class AuthService {
  async login (req: Request, res: Response): Promise<void> {
    const credentials: IAuthCredentials = req.body
    try {
      // TODO: Replace it with firebase cliente (Or replace firebase)
      const response = await Axios.post(
        `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${process.env.FIREBASE_API_KEY}`,
        credentials
      )
      const payload: IAuthPayload = response.data
      res.status(200).send(payload)
    } catch (err) {
      console.log(err.message)
      const axiosError = err.response.data.error
      if (axiosError) {
        console.log(axiosError)
      }
      const errorCode = axiosError.code
      res.sendStatus(errorCode || 500)
    }
  }

  async register (req: Request, res: Response): Promise<void> {
    const newUser = req.body
    try {
      const response = await auth().createUser(newUser)
      const { uid } = response
      database().ref(`Users/${uid}`).set({
        firstName: newUser.firstName,
        lastName: newUser.lastName,
        birth: newUser.birth,
        email: newUser.email
      })
      res.status(201).send(response)
    } catch (err) {
      console.log(err.code)
      res.sendStatus(firebaseAuthErrors[err.code] || 500)
    }
  }
}
