import { Request, Response } from 'express'

export default abstract class AbstractService {
  abstract create(req: Request, res: Response): Promise<void>

  abstract findAll(req: Request, res: Response): Promise<void>

  abstract findById(req: Request, res: Response): Promise<void>

  abstract updateById(req: Request, res: Response): Promise<void>

  abstract deleteById(req: Request, res: Response): Promise<void>
}
