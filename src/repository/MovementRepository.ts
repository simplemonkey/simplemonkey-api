import { getConnection, UpdateResult, DeleteResult, InsertResult, MoreThan } from 'typeorm'
import { subMonths } from 'date-fns'
import { splitWhen } from 'ramda'
import Movement from '../entity/Movement'
import AbstractRepository from './AbstractRepository'

export default class MovementRepository extends AbstractRepository<Movement, string> {
  public create (movement: Movement): Promise<InsertResult> {
    return getConnection()
      .getRepository(Movement)
      .insert(movement)
  }

  public getAll (uid: string): Promise<Movement[]> {
    return getConnection()
      .getRepository(Movement)
      .find({
        relations: ['category'],
        where: { uid }
      })
  }

  public getById (id: string, uid: string): Promise<Movement | undefined> {
    return getConnection()
      .getRepository(Movement)
      .findOne({
        relations: ['category'],
        where: { id, uid }
      })
  }

  public updateById (id: string, movement: Movement): Promise<UpdateResult> {
    return getConnection()
      .getRepository(Movement)
      .update({ id }, movement)
  }

  public deleteById (id: string): Promise<DeleteResult> {
    return getConnection()
      .getRepository(Movement)
      .delete({ id })
  }

  public getLastSixMonths (uid: string): Promise<Movement[]> {
    return getConnection()
      .getRepository(Movement)
      .find({
        relations: ['category'],
        where: { uid, date: MoreThan(subMonths(new Date(), 6)) }
      })
  }

  public getLastTen (uid: string): Promise<Movement[]> {
    return getConnection()
      .getRepository(Movement)
      .find({
        take: 10,
        relations: ['category'],
        where: { uid }
      })
  }

  public getLastMonth (uid: string): Promise<Movement[]> {
    return getConnection()
      .getRepository(Movement)
      .find({
        relations: ['category'],
        where: { uid, date: MoreThan(subMonths(new Date(), 1)) }
      })
  }

  public async getBalance (uid: string): Promise<{ income: number, expense: number }> {
    const data: Movement[] = await getConnection()
      .getRepository(Movement)
      .find({
        relations: ['category'],
        where: { uid }
      })

    const splittedMovements = splitWhen<Movement>(movement => !!movement.income)(data)
    const balance = {
      income: splittedMovements[0].reduce((acc, movement) => movement.amount + acc, 0),
      expense: splittedMovements[1].reduce((acc, movement) => movement.amount + acc, 0)
    }
    return balance
  }
}
