import { UpdateResult, DeleteResult, InsertResult } from 'typeorm'

export default abstract class AbstractRepository<TEntity, TId> {
  abstract create(entity: TEntity): Promise<InsertResult>

  abstract getAll(uid?: string): Promise<TEntity[]>

  abstract getById(id: TId, uid?: string): Promise<TEntity | undefined>

  abstract updateById(id: TId, entity: TEntity): Promise<UpdateResult>

  abstract deleteById(id: TId): Promise<DeleteResult>
}
