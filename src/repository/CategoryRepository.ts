import { getConnection, UpdateResult, DeleteResult, InsertResult } from 'typeorm'
import Category from '../entity/Category'
import AbstractRepository from './AbstractRepository'

export default class CategoryRepository extends AbstractRepository<Category, number> {
  public create (category: Category): Promise<InsertResult> {
    return getConnection().getRepository(Category).insert(category)
  }

  public getAll (): Promise<Category[]> {
    return getConnection().getRepository(Category).find()
  }

  public getById (id: number): Promise<Category | undefined> {
    return getConnection().getRepository(Category).findOne({ where: { id } })
  }

  public updateById (id: number, category: Category): Promise<UpdateResult> {
    return getConnection().getRepository(Category).update({ id }, category)
  }

  public deleteById (id: number): Promise<DeleteResult> {
    return getConnection().getRepository(Category).delete({ id })
  }
}
