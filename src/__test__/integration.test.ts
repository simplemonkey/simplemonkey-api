import app from '../app'
import supertest from 'supertest'
import { createConnection, Connection } from 'typeorm'
import { ICategory } from '../model/ICategory'
const request = supertest(app)

describe('Testing Database connection with TypeORM', () => {
  let connection:Connection
  let category:ICategory
  jest.setTimeout(10000)

  beforeAll(async done => {
    category = { id: 999, name: 'test', color: '#000000', icon: 'test' }
    try {
      connection = await createConnection()
      done()
    } catch(e) {
      console.log('ERROR TYPEORM', e)
      done()
    }
  })

  afterAll(async done => {
    await connection?.close()
    done()
  })

  it('Should fail when trying to post an empty body', async done => {
    const res = await request.post('/categories')
    expect(res.status).toBe(400)
    done()
  })

  it('Should add the category with id 999', async done => {
    const res = await request.post('/categories').send(category)
    expect(res.status).toBe(201)
    done()
  })

  it('Should return 409 code when try to add a duplicate category', async done => {
    const res = await request.post('/categories').send(category)
    expect(res.status).toBe(409)
    done()
  })

  it('Should return all categories', async done => {
    const res = await request.get('/categories')
    expect(res.status).toBe(200)
    done()
  })

  it('Should return the category with id 999', async done => {
    const res = await request.get('/categories/999')
    expect(res.status).toBe(200)
    expect(res.body).toMatchObject({ id: 999 })
    done()
  })

  it('Should return error 404 when category doesnt exist', async done => {
    const res = await request.get('/categories/1001')
    expect(res.status).toBe(404)
    done()
  })

  it('Should update the category with id 999', async done => {
    const res = await request.put('/categories/999').send({ id: 999, name: 'test edited', description: '', color: '#000000', icon: 'test', expense: false, income: false })
    expect(res.status).toBe(204)
    done()
  })

  it('Should return 404 when try to update a non exist category', async done => {
    const res = await request.put('/categories/1001').send({ id: 1001, name: 'test edited', description: '', color: '#000000', icon: 'test', expense: false, income: false })
    expect(res.status).toBe(404)
    done()
  })

  it('Should return 400 when try to update with invalid payload', async done => {
    const res = await request.put('/categories/1001').send({})
    expect(res.status).toBe(400)
    done()
  })

  it('Should delete the category with id 999', async done => {
    const res = await request.delete('/categories/999')
    expect(res.status).toBe(204)
    done()
  })

  it('Should return 404 when try to delete a non exist category', async done => {
    const res = await request.delete('/categories/999')
    expect(res.status).toBe(404)
    done()
  })
  
})

describe('Test Internal Server errors', () => {
  it('Get without database connected must send error 500', async done => {
    const res1 = await request.get('/categories')
    expect(res1.status).toBe(500)
    const res2 = await request.get('/categories/999')
    expect(res2.status).toBe(500)
    done()
  })

  it('Post without database connected must send error 500', async done => {
    const res = await request.post('/categories').send({ id: 999, name: 'test', color: '#000000', icon: 'test' })
    expect(res.status).toBe(500)
    done()
  })

  it('Put without database connected must send error 500', async done => {
    const res = await request.put('/categories/999').send({ id: 999, name: 'test edited', color: '#000000', icon: 'test' })
    expect(res.status).toBe(500)
    done()
  })

  it('Delete without database connected must send error 500', async done => {
    const res = await request.delete('/categories/999')
    expect(res.status).toBe(500)
    done()
  })
})
