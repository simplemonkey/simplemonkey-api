import app from '../app'
import supertest from 'supertest'
const request = supertest(app)

describe('Testing base app', () => {
  it('Should show welcome message', async done => {
    const res = await request.get('/')
    expect(res.status).toBe(200)
    expect(res.body.message).toEqual('Bienvenido a SimpleMonkey API')
    done()
  })
  it('Should return empty object and 404 http code', async done => {
    const res = await request.get('/notexists')
    expect(res.status).toBe(404)
    expect(res.body).toEqual({})
    done()
  })
})
