import 'reflect-metadata'
import express, { json } from 'express'
import morgan from 'morgan'
import { createConnection } from 'typeorm'
import admin from 'firebase-admin'

import cors from './middleware/cors'

import routes from './routes'

// Inicialización de express
const app = express()

// Inicialización de firebase
admin.initializeApp({
  credential: admin.credential.cert({
    projectId: process.env.FIREBASE_PROJECT_ID,
    privateKey: process.env.FIREBASE_PRIVATE_KEY,
    clientEmail: process.env.FIREBASE_CLIENT_EMAIL
  }),
  databaseURL: process.env.FIREBASE_DATABASE_URL
})

// Middlewares
app.use(morgan('dev', { skip: () => process.env.NODE_ENV === 'test' }))
app.use(json())
app.use(cors)

// Context Variables
app.set('PORT', process.env.PORT || 4000)

// Routes
app.use('/', routes)

/* istanbul ignore next */
if (process.env.NODE_ENV !== 'test') {
  createConnection()
    .then(() => {
      console.log('Connected to', process.env.TYPEORM_HOST)
      app.listen(app.get('PORT'), () => {
        console.log('Server on port', app.get('PORT'))
      })
    })
    .catch(e => console.log('TypeORM error:', e))
}

export default app
