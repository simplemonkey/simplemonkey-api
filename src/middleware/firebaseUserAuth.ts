import { NextFunction, Request, Response } from 'express'
import { auth } from 'firebase-admin'

export async function verifyToken (req: Request, res: Response, next: NextFunction): Promise<void> {
  const token: string = req.headers.authorization || ''
  try {
    const decodedToken = await auth().verifyIdToken(token)
    res.locals.uid = decodedToken.uid
    next()
  } catch (error) {
    if (process.env.NODE_ENV === 'develop') {
      console.log('Bad token, but develop bypassing')
      console.log('Setting testing account ID for develop: vltncbqz2vMsI8PXkk1rdVpwX5q1')
      res.locals.uid = 'vltncbqz2vMsI8PXkk1rdVpwX5q1'
      console.log('test@gmail.com | 123456')
      next()
    } else {
      switch (error.code) {
        case 'auth/id-token-expired':
          res.status(403).send({ message: 'Token expired' })
          break
        case 'auth/argument-error':
          res.status(401).send({ message: 'Invalid token' })
          break
        default:
          res.status(403).send(error)
      }
    }
  }
}

export function verifyUid (req: Request, res: Response, next: NextFunction): void {
  const { uid } = req.params
  if (res.locals.uid === uid) {
    next()
  } else {
    res.status(403).send({ message: 'Invalid user id' })
  }
}
