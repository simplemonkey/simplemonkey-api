import { ICategory } from './ICategory'

export interface IMovement {
  id?: string
  uid: string
  name: string
  description?: string
  date: string
  amount: number
  currency: string
  income?: boolean
  done?: boolean
  coordinates?: string
  feeNumber?: number
  payday?: number
  category?: ICategory
}
