export interface IAuthCredentials {
  email: string
  password: string
}

export interface IAuthPayload {
  kind: string
  localId: string
  email: string
  displayName: string
  idToken: string
  registered: boolean
}
