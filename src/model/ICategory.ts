export interface ICategory {
  id: number
  name: string
  description?: string
  color: string
  icon: string
  expense?: boolean
  income?: boolean
}
