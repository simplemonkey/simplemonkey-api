/* istanbul ignore file */
module.exports = {
  name: 'default',
  type: 'mysql',
  host: process.env.TYPEORM_HOST,
  port: process.env.TYPEORM_PORT,
  username: process.env.TYPEORM_USERNAME,
  password: process.env.TYPEORM_PASSWORD,
  database: process.env.TYPEORM_DATABASE,
  timezone: '+00:00',
  dateStrings: true,
  synchronize: true,
  logging: false,
  entities: [
    `${process.env.NODE_ENV === 'production' ? 'build/entity/*.js' : 'src/entity/*.ts'}`
  ],
  cli: {
    entitiesDir: `${process.env.NODE_ENV === 'production' ? 'build' : 'src'}/entity`
  }
}
